#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOCAL_DIR=$(basename $SCRIPT_DIR)
IMAGE_NAME="iomoss/$LOCAL_DIR"

# Create repository
REPOSITORY_NAME=$LOCAL_DIR
curl -u 'IOMOSS-Automata:TEMP1234567890a' https://api.github.com/orgs/iomoss/repos -d "{\"name\":\"$REPOSITORY_NAME\", \"auto_init\":true}"

# Create output directory
mkdir -p build

declare -A ARCH
ARCH[amd64]=debian:jessie
ARCH[i386]=32bit/debian:jessie
ARCH[armv7]=armv7/armhf-debian:jessie

# Generate the README.md
cp README.md.in build/README.md
for i in "${!ARCH[@]}"; do
    IMAGE_NAME=iomossautomata/$LOCAL_DIR:$i
    echo " * **$i**: \`$IMAGE_NAME\`" >> build/README.md
done

for i in "${!ARCH[@]}"; do
    # Get the current architecture
    CURRENT_ARCH=${ARCH[$i]}
    OUTPUT_DIR=build/$i
    # Clone the respository
    git clone git@github.com:iomoss/$REPOSITORY_NAME.git $OUTPUT_DIR
    cd $OUTPUT_DIR
    git checkout -b $i
    git checkout $i
    cd $SCRIPT_DIR
    # Prepare the dockerfile
    echo "FROM $CURRENT_ARCH" | cat - Dockerfile.in > $OUTPUT_DIR/Dockerfile
    # Output the README.md
    IMAGE_NAME=iomossautomata/$LOCAL_DIR:$i
    cat build/README.md | sed "s#!!!IMAGE_NAME!!!#$IMAGE_NAME#g" > $OUTPUT_DIR/README.md
    # Copy all resources
    cp res/* $OUTPUT_DIR/
    # Add and commit all changes
    cd $OUTPUT_DIR 
    git add *
    git commit -am "Automatic update"
    git push -u origin ${i}
    cd $SCRIPT_DIR
done
